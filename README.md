# FRAMEWORK CSS

## Guide d'utilisation

Quatre variables sont à définir dans le fichier `scss/framework.scss` : 

 - $couleur_primaire : la couleur principale du framework
 - $couleur_primaire_dark : la version foncée de la couleur principale du framework
 - $couleur_primaire_light : la version claire de la couleur principale du framework
 - $couleur_secondaire : la couleur secondaire du framework

Le framework est codé en SASS, il faut donc (une fois les variables définies) le compiler en CSS.

L'ensemble des fonctionnalités du framework est rassemblé sur la page `framework.html`

